# **Contents**
- [Description](#Description)
- [Getting Started](#How-it-works)
- [Architecture Diagram](#Architecture-Diagram)
- [Architecture](#Architecture)
  * [VPC](#VPC)
  * [Inernet Gateway](#Internet-Gateway)
  * [NAT Gateway](#NAT-Gateway)
  * [Subnets](#Subnets)
  * [Route Tables](#Route-Tables)
  * [EKS](#EKS)
  * [Security Groups](#Security-Groups)

# **Description**
A terraform code that provisions a full infrastructure on AWS: VPC, Network, Security group and EKS cluster.

# **How it works**
**1. Change directory to main in aws directory**
```
cd  aws/main
```
**2. Plan to check resources that will be created by passing .tfvars file that includes enviroment variables**
```
terraform plan --var-file terraform.tfvars
```
**3. Apply terraform code**
```
terraform apply --var-file terraform.tfvars
```
# **Architecture Diagram**
![architecture](architecture.jpeg)
# **Architecture**
## **VPC**
A virtual private cloud **VPC** is a virtual network dedicated to your AWS account. It is logically isolated from other virtual networks in the AWS Cloud. You can launch your AWS resources, such as Amazon EC2 instances, into your VPC.

## **Internet Gateway**
An internet gateway is a horizontally scaled, redundant, and highly available VPC component that allows communication between our VPC and the internet.

An internet gateway serves two purposes: to provide a target in our VPC route tables for internet-routable traffic, and to perform network address translation **NAT** for instances that have been assigned public IPv4 addresses.

An internet gateway supports IPv4 and IPv6 traffic. It does not cause availability risks or bandwidth constraints on our network traffic.

## **NAT Gateway**
A NAT gateway is a Network Address Translation **NAT** service. We use a NAT gateway so that instances in a private subnet can connect to services outside our VPC but external services cannot initiate a connection with those instances.

## **Subnets**
A subnet is a range of IP addresses in your VPC. We launch AWS resources, such as EC2 instances, into a specific subnet. When We create a subnet, we specify the IPv4 CIDR block for the subnet, which is a subset of the VPC CIDR block. Each subnet must reside entirely within one Availability Zone and cannot span zones. By launching instances in separate Availability Zones, to protect our applications from the failure of a single zone.

### In our VPC we have 6 subnets:
* **Three Public**  We access all private instances, EKS, etc through a bastion server that is launched in the public subnets.
* **Three Private**  The EKS cluster and all addOns are launched in the private subnets to block public access to them to make them secure.

## **Route Tables**
A route table contains a set of rules, called routes, that are used to determine where network traffic from our subnet or gateway is directed.
Each subnet in the VPC must be
associated with a route table, which controls the routing for the subnet.

We have all public subnets are associated with the same route table in which the destination is the internet and the target is the internet gateway.
We also have all private subnets are associated with a different route table in which the destination is also the internet but the target is the
Nat gateway.
***
## **EKS**
Amazon Elastic Kubernetes Service **Amazon EKS** is a managed container service to run and scale Kubernetes applications in the cloud or on-premises.

### **1. EKS Cluster**
The EKS cluster is launched in 6 subnets 3 public and 3 private, the public subnets are for the load balancers and the private for the application to protect them from public access.
### **2. Node Groups**
Node groups are the worker nodes that are responsible for launching the EC2 instances for the applications pods, We launch them in the private subnets.

We have 2 Node Groups:
* **tools_node_group** AddOns/Tools are placed on this node group through taint and tolleration.

* **apps_node_group**
MicroServices are placed on this node group through taint and tolleration in order to isolate them from any damage 

### **3. KMS**
KMS keys is used to provide envelope encryption of Kubernetes secrets stored in Amazon Elastic Kubernetes Service **EKS**. Implementing envelope encryption is considered a security best practice for applications that store sensitive data and is part of a defense in depth security strategy.

### **4. ECR**
Amazon ECR is a fully managed container registry offering high-performance hosting, so you can reliably deploy application images and artifacts anywhere.

We use **ECR** with **EKS** to store our applications images.
***
## **Security Groups**
We have Role for EKS Worker Nodes that a has policies attached to it:

**1. EKS Roles and Policeies**
* Policy that allow it to launch ec2 
* Policy for autoscaling in order to be able to launch or terminate ec2 instances depends on the load.
* Policy for accessing ECR in order to be able to pull images.

**2. Bastion Server secuirty group**
* Open port 22 to allow ssh 

